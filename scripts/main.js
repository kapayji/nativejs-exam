var buttons = document.getElementById("buttons-wrapper");
var answersBlock = document.getElementById("answers");
var dataJson = [];
var questionNumber = 0;
var outcome = [];
var answered = new Set();
var isFew = false;
var isOwnAnswer = false;
var ownAnswer = "";

class MainElement {
  constructor(typeName, id, className, text, optionsName, options) {
    this.typeName = typeName;
    this.id = id;
    this.className = className;
    this.text = text;
    this.optionsName = optionsName;
    this.options = options;
  }
  create() {
    let newFuncElement = document.createElement(this.typeName);
    newFuncElement.setAttribute("id", this.id);
    newFuncElement.setAttribute("class", this.className);
    newFuncElement.innerHTML = this.text;
    newFuncElement.setAttribute(this.optionsName, this.options);
    return newFuncElement;
  }
}

//Get data from json
function getDataFromJson() {
  fetch("./src/container.JSON")
    .then((response) => response.json())
    .then((json) => (dataJson = json));
}
//Start
function start() {
  buttons.appendChild(
    new MainElement(
      "button",
      "tempStartBtn",
      "temp",
      "Начать",
      "onclick",
      "createButtons()"
    ).create()
  );
}
start();

//
function fillField(numQ) {
  //call fetch
  getDataFromJson();
  // first check
  var numQuest =
    numQ !== undefined
      ? numQ
      : localStorage.getItem("test-quiz")
      ? localStorage.getItem("test-quiz")
      : 0;
  //remove previous elements
  clearTempElements();
  answersBlock.appendChild(fakeSpinner());
  //fill question field
  setTimeout(() => {
    function answers(num) {
      //
      //remove previous elements
      clearTempElements();
      //get info from json
      let ansBlock = dataJson[num].answers[0];
      var questNum = document.getElementById("quest_num");
      questNum.innerHTML = `Вопрос №${dataJson[num].id} из ${dataJson.length}`;
      var ansBody = document.getElementById("ans_body");
      ansBody.innerHTML = dataJson[num].body;
      //
      //check buttons
      checkBtn(num, dataJson.length - 1);
      //////put in LS
      localStorage.setItem("test-quiz", num);
      questionNumber = dataJson[num].id;
      isFew = dataJson[num].few_answers;
      isOwnAnswer = dataJson[num].own_answer;
      ////////////////////
      Object.keys(ansBlock).map((el) => {
        createInputAnswer(ansBlock, el);
      });
      ///
      isOwnAnswer ? createOwnAnswerArea() : null;
    }
    answers(numQuest);
  }, 1000);
}

//create buttons
function createButtons() {
  setTimeout(() => {
    //button Previous
    buttons.appendChild(
      new MainElement(
        "button",
        "prev_question-btn",
        "main__button-prev",
        "Предыдущий вопрос",
        "onclick",
        "prevQuestion()"
      ).create()
    );

    //button Answer
    buttons.appendChild(
      new MainElement(
        "button",
        "answer-btn",
        "main__button-ans",
        "Ответить",
        "onclick",
        "answerQuestion()"
      ).create()
    );
    //button Next
    buttons.appendChild(
      new MainElement(
        "button",
        "next_question-btn",
        "main__button-next",
        "Следующий вопрос",
        "onclick",
        "nextQuestion()"
      ).create()
    );
    //remove temp button
    document.getElementById("tempStartBtn").remove();
    fillField();
  }, 1000);
}
//

//buttons functions
function prevQuestion() {
  let numNow = Number(localStorage.getItem("test-quiz"));
  numNow >= 1 ? fillField(numNow - 1) : numNow;
}
function nextQuestion() {
  let numNow = Number(localStorage.getItem("test-quiz"));
  numNow < dataJson.length - 1
    ? fillField(numNow + 1)
    : (clearTempElements(), resultTest());
}
function answerQuestion() {
  let selectionValue = document.getElementsByName("answer");
  let tempResult = [];
  ownAnswer = "";
  isFew
    ? selectionValue.forEach((v) => (v.checked ? tempResult.push(v.value) : v))
    : selectionValue.forEach((v) => (v.checked ? tempResult.push(v.value) : v));
  ownAnswer.length !== 0 ? tempResult.push(ownAnswer) : "";
  outcome.push({
    id: questionNumber,
    answer: `На вопрос №${questionNumber} пользователь ответил ${tempResult}`,
    answered: true,
    details: [],
  });
  outcome.forEach((v) =>
    v.id == questionNumber ? tempResult.forEach((o) => v.details.push(o)) : v
  );
  answered.add(questionNumber);
  sortArray(outcome, outcome.id);
  nextQuestion();
}
// check buttons disabling
function checkBtn(numPrev, numNext) {
  numPrev === 0
    ? document
        .getElementById("prev_question-btn")
        .setAttribute("disabled", true)
    : document
        .getElementById("prev_question-btn")
        .removeAttribute("disabled", true);
  numPrev === numNext
    ? document
        .getElementById("next_question-btn")
        .setAttribute("disabled", true)
    : document
        .getElementById("next_question-btn")
        .removeAttribute("disabled", true);
}

function createInputAnswer(ansBlock, el) {
  let ans = ansBlock[el];
  let radioBtn = document.createElement("input");
  let label = document.createElement("label");
  let descr = document.createTextNode(ans);
  let separateLine = document.createElement("br");
  let ansBtn = document.getElementById("answer-btn");
  ///////////
  isFew ? (radioBtn.type = "checkbox") : (radioBtn.type = "radio");
  //////////
  radioBtn.name = "answer";
  radioBtn.id = "answer";
  radioBtn.value = ans;
  answered.has(questionNumber)
    ? ((radioBtn.disabled = true), (ansBtn.disabled = true))
    : (ansBtn.disabled = false);
  outcome.forEach((v) =>
    v.details.forEach((o) =>
      o == radioBtn.value ? (radioBtn.checked = true) : o
    )
  );
  radioBtn.setAttribute("class", "input__answer temp");
  ///////////////
  separateLine.setAttribute("class", "temp");
  ///////////////
  label.htmlFor = "answer";
  label.setAttribute("class", "label__answer temp");
  label.appendChild(descr);
  ////////////////
  answersBlock.appendChild(radioBtn);
  answersBlock.appendChild(label);
  answersBlock.appendChild(separateLine);
}

function createOwnAnswerArea() {
  let ownAnswerArea = document.createElement("textarea");
  ownAnswerArea.maxLength = 10;
  ownAnswerArea.placeholder = "Не более 10 символов";
  ownAnswerArea.setAttribute("class", "temp");
  ownAnswerArea.setAttribute("id", "own_answer_area");
  ownAnswerArea.style.resize = "none";
  answered.has(questionNumber) ? (ownAnswerArea.disabled = true) : null;
  let title = document.createElement("p");
  title.setAttribute("class", "temp");
  title.innerHTML = "Введите свой вариант";
  answersBlock.appendChild(title);
  answersBlock.appendChild(ownAnswerArea);
  onChangeTextArea();
}

function clearTempElements() {
  [...document.getElementsByClassName("temp")].map((v) => v && v.remove());
}

function onChangeTextArea() {
  let textArea = document.getElementById("own_answer_area");
  let inputs = document.querySelectorAll("input");
  textArea.addEventListener("input", (e) => {
    e.target.value.length !== 0
      ? (inputs.forEach((v) => (v.disabled = true)),
        (ownAnswer += e.target.value))
      : inputs.forEach((v) => (v.disabled = false));
  });
}
//Spinner imitation
function fakeSpinner() {
  let spinner = document.createElement("div");
  spinner.setAttribute("class", "lds-dual-ring temp");
  return spinner;
}

//Result screen
function resultTest() {
  let resultScreen = document.createElement("ul");
  resultScreen.setAttribute("class", "temp");
  sortArray(outcome, outcome.id);
  outcome.forEach((v) => {
    let resultScreenElement = document.createElement("li");
    resultScreenElement.innerHTML = v.answer;
    resultScreen.appendChild(resultScreenElement);
  });
  answersBlock.appendChild(resultScreen);
}
//sort
function sortArray(arr, key) {
  return arr.sort((a, b) => {
    let x = a[key];
    let y = b[key];
    x < y ? -1 : x > y ? 1 : 0;
  });
}
